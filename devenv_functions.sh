#!/bin/bash

YELLOW='\e[93m'
RED='\e[91m'
ENDCOLOR='\033[0m'
CYAN='\e[96m'
GREEN='\e[92m'
SCRIPTPATH=$(pwd)

function pause(){
   read -p "$*"
}

function checksudo() {
  if [ "$EUID" -ne 0 ]
  then
    echo
    echo -e $RED'Please run the script using sudo.'$ENDCOLOR
    echo
    exit 0
  fi
}

function header(){
  if [ "$header_shown" != 'true' ]
  then
  clear
    echo -e $CYAN
    echo -e "            _____             ______             "
    echo -e "           |  __ \           |  ____|            "
    echo -e "           | |  | | _____   _| |__   _ ____   __ "
    echo -e "           | |  | |/ _ \ \ / /  __| | '_ \ \ / / "
    echo -e "           | |__| |  __/\ V /| |____| | | \ V /  "
    echo -e "           |_____/ \___| \_/ |______|_| |_|\_/   "
    echo
    echo
    echo -e $GREEN"(X)Ubuntu 14.04 LTS Development Environment Setup Script(s)"$ENDCOLOR
    echo
    echo -e "NOTE: At this point, this script has been confirmed to work only on XUbuntu 14.04 LTS."
    echo

    export header_shown='true';
  fi
}

function install(){
  if [ -z "$1" ]                           # Is parameter #1 zero length?
  then
    echo -e $RED"call to install() - parameter is zero length or empty."$ENDCOLOR
  else
    package_status=$(dpkg-query -W --showformat='${Status}\n' $1|grep "install ok installed")

    if [ "" == "$package_status" ]
    then
      echo -e $GREEN"Installing:"$CYAN"$1"$ENDCOLOR
      sudo apt-get --assume-yes install $1
    else
      echo -e $YELLOW"Not installing:"$CYAN"$1" $YELLOW"Status:"$CYAN"$package_status" $ENDCOLOR
    fi
  fi
}

function purge(){
  if [ -z "$1" ]                           # Is parameter #1 zero length?
  then
    echo -e $RED"call to purge() - parameter is zero length or empty."$ENDCOLOR
  else
    package_status=$(dpkg-query -W --showformat='${Status}\n' $1|grep "install ok installed")

    if [ "" == "$package_status" ]
    then
      echo -e $YELLOW"Not purging:"$CYAN"$1" $YELLOW" It is not installed."$ENDCOLOR
    else
      echo -e $GREEN"Purging:"$CYAN"$1"$ENDCOLOR
      sudo apt-get purge --assume-yes $1
    fi
  fi
}
