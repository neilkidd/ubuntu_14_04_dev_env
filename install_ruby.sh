#!/bin/bash
git clone https://github.com/sstephenson/rbenv.git ~/.rbenv

#echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
#for ubuntu desktop envs
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc

#echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
echo 'eval "$(rbenv init -)"' >> ~/.bashrc

git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
