mkdir ~/packer
cd ~/packer
wget https://releases.hashicorp.com/packer/0.10.0/packer_0.10.0_linux_amd64.zip
unzip ./packer_0.10.0_linux_amd64.zip
rm packer*.zip
cd ~/

echo 'export PATH="$PATH:$HOME/packer"' >> ~/.bashrc
