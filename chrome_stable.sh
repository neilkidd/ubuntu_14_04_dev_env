wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

# After installing the key, run the below command to add it to the repository.

sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

# Thats it. Now update your repository and install the browser.
sudo apt-get update
sudo apt-get install -y google-chrome-stable
