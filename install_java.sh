# https://www.digitalocean.com/community/tutorials/how-to-install-java-on-ubuntu-with-apt-get
# http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update

sudo debconf-set-selections <<< 'oracle-java8-installer shared/accepted-oracle-license-v1-1 select true'
sudo apt-get install -y oracle-java8-installer
sudo update-java-alternatives -s java-8-oracle


# sudo debconf-set-selections <<< 'oracle-java7-installer shared/accepted-oracle-license-v1-1 select true'
# sudo apt-get install -y oracle-java7-installer
# sudo update-java-alternatives -s java-7-oracle
