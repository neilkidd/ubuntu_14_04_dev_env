#!/bin/bash

sudo apt-get --assume-yes update && sudo apt-get --assume-yes upgrade
purge "avahi-daemon"; #conflicts with 7digital Windows AD .local domain
purge "gnumeric";
purge "abiword";
purge "thunderbird";

sudo apt-get --assume-yes autoremove
sudo apt-get --assume-yes autoclean
sudo apt-get --assume-yes clean

debconf-set-selections <<< 'ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true'
install "xubuntu-restricted-extras";

install "libavcodec-extra";
install "aptitude";
install "synaptic";
install "gdebi-core";
install "zlib1g-dev";
install "build-essential";
install "libssl-dev";
install "libreadline-dev";
install "libyaml-dev";
install "libsqlite3-dev";
install "sqlite3";
install "libxml2-dev";
install "libxslt1-dev";
install "libcurl4-openssl-dev";
install "python-software-properties";
install "libffi-dev";
install "autoconf";
install "rar";
install "git-core";
install "git";
install "git-gui";
install "ack-grep";
install "curl";
install "openssh-server";
install "openssh-client";
install "gnome-system-monitor";
install "vlc";
install "gksu";
install "gparted";
install "remmina";
install "libreoffice";
install "tree";
install "geany";
