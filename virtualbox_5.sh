# https://www.virtualbox.org/wiki/Linux_Downloads
sudo -v
echo "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | sudo tee -a /etc/apt/sources.list
wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y virtualbox-5.0

# install the extension pack from
https://www.virtualbox.org/wiki/Downloads

#start up and rename -> preferences -> ~/VirtualBox VMs to ~/VirtualBoxVMs
# I dont like the space - shell scripting and such - YMMV
