#!/bin/bash

. devenv_functions.sh

checksudo;

SCRIPTPATH=$(pwd)

sudo chmod -R 775 * >/dev/null 2>&1

header;

echo -e $YELLOW'00. '$ENDCOLOR'Check and Update Scripts'
echo -e $YELLOW'01. '$ENDCOLOR'Install basic packages'
echo -e $YELLOW'10. '$ENDCOLOR'Install atom editor'
echo -e $YELLOW'11. '$ENDCOLOR"Install node (nvm)"
echo -e $YELLOW'12. '$ENDCOLOR'Install git'
echo -e $YELLOW'13. '$ENDCOLOR"Install ruby (RBenv)"
echo -e $YELLOW'99. '$ENDCOLOR'Exit'
echo
echo -n "What would you like to do [00-99]: "
read option
case $option in
	0 | 00)
		echo
	  echo -e $YELLOW'--->Checking for updates...'$ENDCOLOR
		git pull
		echo
		pause 'Press [Enter] to restart and continue...'
		cd $SCRIPTPATH
		sudo ./setup.sh
		exit 0
		;;
	1 | 01)
         	sudo ./basics.sh
		;;

    10)
    		sudo ./install_atom.sh
    		;;
    11)
    		sudo ./install_node.sh
    		;;
    12)
    		sudo ./install_git.sh
    		;;
	  13)
    		sudo ./install_ruby.sh
    		;;
    99)
    		echo 'Exiting...'
    		echo
    		echo -e $YELLOW'Please contribute :)'$ENDCOLOR
    		echo
    		sleep 2
		    ;;
  *)
        echo -e $RED'Invalid Option'$ENDCOLOR
		    ScriptLoc=$(readlink -f "$0")
		    sleep 1
		    exec $ScriptLoc
esac
