#!/bin/bash

sudo add-apt-repository ppa:webupd8team/atom -y
sudo apt-get update
sudo apt-get install atom -y

# Packages
sudo apm install package-sync
